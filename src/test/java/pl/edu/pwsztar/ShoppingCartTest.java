package pl.edu.pwsztar;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ShoppingCartTest {

    @DisplayName("Should be right add product to shopping cart")
    @ParameterizedTest
    @CsvSource({
            "ProductNumberOne, 15, 3",
            "ProductNumberTwo, 5, 2"
    })
    void rightAddProductToShoppingCart(String productName, int price, int amount) {

        final ShoppingCart shoppingCart = new ShoppingCart();

        final boolean result = shoppingCart.addProducts(productName, price, amount);

        assertTrue(result);
    }

    @DisplayName("Shouldn't be right add product to shopping cart")
    @ParameterizedTest
    @CsvSource({
            "ProductNumberOne, 2, 0",
            "ProductNumberTwo, 0, 2",
            "ProductNumberThree, -5, -10",
            "ProductNumberFour, -5, 0",
            "ProductNumberFive, 0, -5",
            "ProductNumberSix, 0, 0"
    })
    void notRightAddProductToShoppingCart(String productName, int price, int amount) {

        final ShoppingCart shoppingCart = new ShoppingCart();

        final boolean result = shoppingCart.addProducts(productName, price, amount);

        assertFalse(result);
    }

    @DisplayName("Should be right delete product from shopping cart")
    @ParameterizedTest
    @CsvSource({
            "ProductNumberOne, 3",
            "ProductNumberTwo, 2"
    })
    void rightDeleteProductFromShoppingCart(String productName, int amount) {

        final ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("ProductNumberOne", 15, 10);
        shoppingCart.addProducts("ProductNumberTwo", 5, 5);

        final boolean result = shoppingCart.deleteProducts(productName, amount);

        assertTrue(result);
    }

    @DisplayName("Shouldn't be right delete product from shopping cart")
    @ParameterizedTest
    @CsvSource({
            "ProductNumberOne, 12",
            "ProductNumberTwo, 7"
    })
    void notRightDeleteProductFromShoppingCart(String productName, int amount) {

        final ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("ProductNumberOne", 15, 10);
        shoppingCart.addProducts("ProductNumberTwo", 5, 5);

        final boolean result = shoppingCart.deleteProducts(productName, amount);

        assertFalse(result);
    }

    @DisplayName("Should be right return quantity of product from shopping cart")
    @ParameterizedTest
    @CsvSource({
            "ProductNumberOne, 2",
            "ProductNumberTwo, 15"
    })
    void rightReturnQuantityOfProductFromShoppingCart(String productName, int amount) {

        final ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("ProductNumberOne", 15, 2);
        shoppingCart.addProducts("ProductNumberTwo", 5, 15);

        final int result = shoppingCart.getQuantityOfProduct(productName);

        assertEquals(result, amount);
    }

    @DisplayName("Should be right return sum price of all products from shopping cart")
    @Test
    void rightReturnSumOfProductPriceFromShoppingCart() {

        final ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("ProductNumberOne", 15, 4);
        shoppingCart.addProducts("ProductNumberTwo", 5, 4);

        final int result = shoppingCart.getSumProductsPrices();

        assertEquals(80, result);
    }

    @DisplayName("Should be right return price of product from shopping cart")
    @ParameterizedTest
    @CsvSource({
            "ProductNumberOne, 15",
            "ProductNumberTwo, 5"
    })
    void rightReturnProductPriceFromShoppingCart(String productName, int price) {

        final ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("ProductNumberOne", 15, 2);
        shoppingCart.addProducts("ProductNumberTwo", 5, 3);

        final int result = shoppingCart.getProductPrice(productName);

        assertEquals(result, price);
    }

    @DisplayName("Should be right return names of products from shopping cart")
    @Test
    void rightReturnNamesOfProductFromShoppingCart() {

        final ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("ProductNumberOne", 15, 2);
        shoppingCart.addProducts("ProductNumberTwo", 5, 3);

        List<String> products = new ArrayList<>();
        products.add("Productnumberone");
        products.add("Productnumbertwo");

        final List<String> result = shoppingCart.getProductsNames();

        assertEquals(products, result);
    }
}
